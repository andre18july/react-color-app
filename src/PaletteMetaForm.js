import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import "emoji-mart/css/emoji-mart.css";
import { Picker } from "emoji-mart";

class PaletteMetaForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      stage: "form",
      paletteName: ""
    };

    this.setPaletteName = this.setPaletteName.bind(this);
    this.addEmojiAndSavePalette = this.addEmojiAndSavePalette.bind(this);
  }

  componentDidMount() {
    ValidatorForm.addValidationRule("isPaletteNameUnique", value =>
      this.props.palettes.every(
        ({ paletteName }) => paletteName.toLowerCase() !== value.toLowerCase()
      )
    );
  }

  handleChangeStage = () => {
    this.setState({ stage: "emoji" });
  };

  setPaletteName(event) {
    this.setState({
      paletteName: event.target.value

    });
  }

  addEmojiAndSavePalette(emoji) {
    this.props.savePalette(this.state.paletteName, emoji.native);
    this.setState({ stage: "" });
    //console.log(emoji);
  }

  render() {
    const { paletteName, stage } = this.state;
    const { hideSavePaletteForm } = this.props;

    return (
      <div>
        <Dialog open={stage === "emoji"} onClose={hideSavePaletteForm}>
          <DialogTitle id="form-dialog-title">Pickup palette emoji</DialogTitle>
          <Picker
            onSelect={this.addEmojiAndSavePalette}
            title="Pickup palette emoji"
          />
        </Dialog>
        <Dialog
          open={stage === "form"}
          onClose={hideSavePaletteForm}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Save Palette</DialogTitle>

          <ValidatorForm onSubmit={this.handleChangeStage}>
            <DialogContent>
              <DialogContentText>
                Please enter a name for your new customized palette. Make sure
                it's unique!
              </DialogContentText>
            </DialogContent>

            <TextValidator
              label="Palette Name"
              value={paletteName}
              onChange={this.setPaletteName}
              validators={["required", "isPaletteNameUnique"]}
              fullWidth
              margin="normal"
              errorMessages={[
                "This field is required",
                "Palette name is not unique."
              ]}
            />

            <DialogActions>
              <Button onClick={hideSavePaletteForm} color="primary">
                Cancel
              </Button>
              <Button variant="contained" type="submit" color="primary">
                Save
              </Button>
            </DialogActions>
          </ValidatorForm>
        </Dialog>
      </div>
    );
  }
}

export default PaletteMetaForm;
