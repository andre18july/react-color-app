import React, { Component } from "react";
import ColorBox from "./ColorBox";
import Navbar from "./Navbar";
import PaletteFooter from "./PaletteFooter";
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/styles";
import styles from './styles/PaletteStyles';

class SingleColorPalette extends Component {
  constructor(props) {
    super(props);
    this._shades = this.gatherShades();
    this.state = {
      format: "hex"
    };
    this.changeFormat = this.changeFormat.bind(this);
  }

  gatherShades() {
    const { palette, idColor } = this.props;
    let tempColors = [];
    for (let key in palette.colors) {
      tempColors = tempColors.concat(
        palette.colors[key].filter(color => color.id === idColor)
      );
    }

    return tempColors.slice(1);
  }

  changeFormat(formatVal) {
    this.setState({
      format: formatVal
    });
  }

  render() {
    const { format } = this.state;
    const { classes } = this.props;
    const { paletteName, emoji, id } = this.props.palette;

    return (
      <div className={classes.Palette}>
        <Navbar changeFormat={this.changeFormat} />
        <div className={classes.paletteColors}>
          {this._shades.map(color => (
            <ColorBox
              key={color.name}
              background={color[format]}
              name={color.name}
              showingFullPalette={false}
            />
          ))}
          <div className={classes.colorBoxBack}>
            <Link to={`/palette/${id}`}>Go Back</Link>
          </div>
        </div>
        <PaletteFooter paletteName={paletteName} emoji={emoji} />
      </div>
    );
  }
}

export default withStyles(styles)(SingleColorPalette);
