import React from "react";
import DraggableColorBox from "./DraggableColorBox";
import { SortableContainer } from "react-sortable-hoc";

const draggableColorList = SortableContainer(props => {
  const { colors } = props;

  return (
    <div style={{ height: "100%" }}>
      {colors.map((color, i) => (
        <DraggableColorBox
          key={color.name}
          color={color}
          index={i}
          deleteColor={() => props.deleteColor(color.name)}
        />
      ))}
    </div>
  );
});

export default draggableColorList;
