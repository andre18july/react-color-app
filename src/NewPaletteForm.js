import React, { Component } from "react";
import classNames from "classnames";
import { withStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import Button from "@material-ui/core/Button";
import DraggableColorList from "./DraggableColorList";
import { arrayMove } from "react-sortable-hoc";
import PaletteFormNav from "./PaletteFormNav";
import ColorPickerForm from "./ColorPickerForm";
import styles from './styles/NewPaletteFormStyles';
import seedColors from "./seedColors";

class NewPaletteForm extends Component {
  static defaultProps = {
    maxColors: 20
  };
  constructor(props) {
    super(props);


    this.state = {
      open: true,
      colors: seedColors[0].colors
    };

    this.handleAddColor = this.handleAddColor.bind(this);
    this.deleteColor = this.deleteColor.bind(this);
    this.clearPalette = this.clearPalette.bind(this);
    this.getRandomColor = this.getRandomColor.bind(this);
    this.savePalette = this.savePalette.bind(this);
  }

  handleDrawerOpen = () => {
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };

  handleAddColor(name, color) {
    this.setState({
      colors: [
        ...this.state.colors,
        {
          name: name,
          color: color
        }
      ]
    });
  }

  deleteColor(colorName) {
    const updatedColors = this.state.colors
      .filter(color => color.name.toLowerCase() !== colorName.toLowerCase())
      .map(filtColor => ({ ...filtColor }));
    console.log(updatedColors);

    this.setState({
      colors: updatedColors
    });
  }

  onSortEnd = ({ oldIndex, newIndex }) => {
    this.setState(({ colors }) => ({
      colors: arrayMove(colors, oldIndex, newIndex)
    }));
  };

  clearPalette() {
    console.log("clear palette");
    this.setState({
      colors: []
    });
  }

  getRandomColor() {
    const allColors =  this.props.palettes.map(p => p.colors).flat();
    let rand;
    let randomColor;

    let isDuplicateColor = true;
    
    while(isDuplicateColor){
      rand = Math.floor(Math.random() * allColors.length);
      randomColor = allColors[rand];
      isDuplicateColor = this.state.colors.some(color => color.name.toLowerCase() === randomColor.name.toLowerCase());
    }

    
    this.setState({
      colors: [...this.state.colors, randomColor ]
    });
  }

  savePalette(paletteName, emoji) {
    const newPalette = {
      paletteName: paletteName,
      id: paletteName.toLowerCase().replace(/ /g, "-"),
      emoji: emoji,
      colors: this.state.colors
    };
    console.log('CCCCCCCC');
    this.props.savePalette(newPalette);
    this.props.history.push(`/`);
  }

  render() {
    const { classes, palettes } = this.props;
    const { open } = this.state;

    const paletteIsFull =
      this.state.colors.length >= this.props.maxColors ? true : false;

    return (
      <div className={classes.root}>
        <PaletteFormNav
          open={open}
          palettes={palettes}
          savePalette={this.savePalette}
          handleDrawerOpen={this.handleDrawerOpen}
        />
        <Drawer
          className={classes.drawer}
          variant="persistent"
          anchor="left"
          open={open}
          classes={{
            paper: classes.drawerPaper
          }}
        >
          <div className={classes.drawerHeader}>
            <IconButton onClick={this.handleDrawerClose}>
              <ChevronLeftIcon />
            </IconButton>
          </div>
          <Divider />

          <div className={classes.container}>
            <Typography variant="h4" gutterBottom>Design Your Palette</Typography>
            <div className={classes.buttons}>
              <Button
                className={classes.button}
                variant="contained"
                color="secondary"
                onClick={this.clearPalette}
              >
                Clear Palette
              </Button>
              <Button
                className={classes.button}
                disabled={paletteIsFull ? true : false}
                variant="contained"
                color="primary"
                onClick={this.getRandomColor}
              >
                Random Color
              </Button>
            </div>

            <ColorPickerForm
              paletteIsFull={paletteIsFull}
              colors={this.state.colors}
              handleAddColor={this.handleAddColor}
            />
          </div>
        </Drawer>
        <main
          className={classNames(classes.content, {
            [classes.contentShift]: open
          })}
        >
          <div className={classes.drawerHeader} />
          <DraggableColorList
            colors={this.state.colors}
            deleteColor={this.deleteColor}
            axis="xy"
            onSortEnd={this.onSortEnd}
            distance={20}
          />
        </main>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(NewPaletteForm);
