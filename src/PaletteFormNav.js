import React, { Component } from "react";
import { Link } from "react-router-dom";
import classNames from "classnames";
import AppBar from "@material-ui/core/AppBar";
import { withStyles } from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import PaletteMetaForm from "./PaletteMetaForm";
import styles from "./styles/PaletteFormNavStyles";
import AddToPhotosIcon from "@material-ui/icons/AddToPhotos";

class PaletteFormNav extends Component {
  constructor(props) {
    super(props);

    this.state = {
      formShowing: false
    };

    this.showSavePaletteForm = this.showSavePaletteForm.bind(this);
    this.hideSavePaletteForm = this.hideSavePaletteForm.bind(this);
  }

  showSavePaletteForm() {
    this.setState({
      formShowing: true
    });
  }

  hideSavePaletteForm() {
    this.setState({
      formShowing: false
    });
  }

  render() {
    const {
      classes,
      open,
      palettes,
      savePalette,
      handleDrawerOpen
    } = this.props;

    return (
      <div className={classes.root}>
        <CssBaseline />
        <AppBar
          position="fixed"
          color="default"
          className={classNames(classes.appBar, {
            [classes.appBarShift]: open
          })}
        >
          <Toolbar disableGutters={!open}>
            <IconButton
              color="inherit"
              aria-label="Open drawer"
              onClick={handleDrawerOpen}
              className={classNames(classes.menuButton, {
                [classes.hide]: open
              })}
            >
              <AddToPhotosIcon />
            </IconButton>

            <Typography variant="h6" color="inherit" noWrap>
              Create a Palette
            </Typography>
          </Toolbar>
          <div className={classes.navBtns}>
            <Link to="/" className={classes.link}>
              <Button
                variant="contained"
                color="secondary"
                className={classes.button}
              >
                Go Back
              </Button>
            </Link>
            <Button
              variant="contained"
              color="primary"
              onClick={this.showSavePaletteForm}
              className={classes.button}
            >
              Save Palette
            </Button>
          </div>
        </AppBar>
        {this.state.formShowing === true ? (
          <PaletteMetaForm
            savePalette={savePalette}
            palettes={palettes}
            hideSavePaletteForm={this.hideSavePaletteForm}
          />
        ) : null}
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(PaletteFormNav);
