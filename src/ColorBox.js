import React, { Component } from "react";
import { CopyToClipboard } from "react-copy-to-clipboard";
import { Link } from "react-router-dom";
import classNames from "classnames";
import { withStyles } from "@material-ui/styles";
import styles from './styles/ColorBoxStyles';


class ColorBox extends Component {
  constructor(props) {
    super(props);

    this.state = {
      copied: false
    };

    this.changeCopyState = this.changeCopyState.bind(this);
  }

  changeCopyState() {
    this.setState({ copied: true }, () => {
      setTimeout(() => {
        this.setState({
          copied: false
        });
      }, 2500);
    });
  }

  render() {
    const color = this.props.background;
    const { name, moreURL, classes, showingFullPalette } = this.props;

    return (
      <CopyToClipboard text={color} onCopy={this.changeCopyState}>
        <div style={{ background: color }} className={classes.ColorBox}>
          <div
            style={{ background: color }}
            className={classNames(classes.copyOverlay, {[classes.showOverlay]: this.state.copied})}
            /*
              Forma alternativa de realizar o className em cima

              className={`${
                this.state.copied ? classes.copyOverlay + " " + classes.showOverlay : classes.copyOverlay
              }`}
            */
            
          />
          <div
            className={`${
              this.state.copied
                ? `${classes.copyMsg} ${classes.showCopyMsg} ${classes.copyText}`
                : classes.copyMsg
            }`}
          >
            <h1>Copied!</h1>
            <p>{color}</p>
          </div>
          <div>
            <div className={classes.boxContent}>
              <span className={classes.copyText}>{name}</span>
            </div>
            <button className={classes.copyButton + " " + classes.copyText}>Copy</button>
          </div>

          {showingFullPalette ? (
            <Link to={moreURL} onClick={e => e.stopPropagation()}>
              <span className={classes.seeMore + " " + classes.copyText}>
                More
              </span>
            </Link>
          ) : null}
        </div>
      </CopyToClipboard>
    );
  }
}

export default withStyles(styles)(ColorBox);
