import React from "react";
import { withStyles } from "@material-ui/styles";
import styles from './styles/PaletteFooterStyles';


const paletteFooter = props => {
  const { paletteName, emoji, classes } = props;

  return (
    <footer className={classes.paletteFooter}>
      {paletteName}
      <span className={classes.footerEmoji}>{emoji}</span>
    </footer>
  );
};

export default withStyles(styles)(paletteFooter);
