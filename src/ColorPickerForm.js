import React, { Component } from "react";
import { ChromePicker } from "react-color";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import Button from "@material-ui/core/Button";
import { withStyles } from "@material-ui/core/styles";
import styles from './styles/ColorPickerFormStyles';

class ColorPickerForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      color: "#884747",
      name: "Start Color"
    };

    this.updateColor = this.updateColor.bind(this);
    this.setColorName = this.setColorName.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    ValidatorForm.addValidationRule("isColorNameUnique", value =>
      /*
      !this.state.colors.find(
        ({ name }) => name.toLowerCase() === value.toLowerCase()
      )
      */
      this.props.colors.every(
        ({ name }) => name.toLowerCase() !== value.toLowerCase()
      )
    );

    ValidatorForm.addValidationRule("isColorValueUnique", () =>
      this.props.colors.every(
        ({ color }) => color.toLowerCase() !== this.state.color
      )
    );
  }

  updateColor(color) {
    this.setState({
      color: color.hex
    });
  }

  setColorName(event) {
    this.setState({
      name: event.target.value
    });
  }

  handleSubmit() {
    this.props.handleAddColor(this.state.name, this.state.color);
    this.setState({
      name: ""
    });
  }

  render() {
    const { name, color } = this.state;
    const { paletteIsFull, classes } = this.props;

    return (
      <div style={{width: "100%"}}>
        <ChromePicker
          color={color}
          onChangeComplete={this.updateColor}
          className={classes.picker}
        />
        {!paletteIsFull ? (
          <ValidatorForm onSubmit={this.handleSubmit} instantValidate={false}>
            <TextValidator
              className={classes.colorName}
              value={name}
              variant="filled"
              margin="normal"
              placeholder="Color Name"
              onChange={this.setColorName}
              validators={[
                "required",
                "isColorNameUnique",
                "isColorValueUnique"
              ]}
              errorMessages={[
                "This field is required",
                "Color name is not unique.",
                "Color value hex is not unique."
              ]}
            />
            <Button
              className={classes.addColor}
              disabled={paletteIsFull}
              variant="contained"
              color="primary"
              style={{ backgroundColor: color }}
              type="submit"
            >
              ADD COLOR
            </Button>
          </ValidatorForm>
        ) : (
          <div style={{ color: "green", fontWeight: "500", width: "100%" }}>
            Palette is complete!
          </div>
        )}
      </div>
    );
  }
}

export default withStyles(styles)(ColorPickerForm);
