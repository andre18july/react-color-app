import React, { Component } from "react";
import MiniPalette from "./MiniPalette";
import { withStyles } from "@material-ui/styles";
import styles from "./styles/PaletteListStyles";
import { Link } from "react-router-dom";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import ListItemText from "@material-ui/core/ListItemText";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import CheckIcon from "@material-ui/icons/Check";
import CloseIcon from "@material-ui/icons/Close";
import blue from "@material-ui/core/colors/blue";
import red from "@material-ui/core/colors/red";

class PaletteList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      paletteId: ''
    }

    this.goToPalette = this.goToPalette.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.deleteConfirm = this.deleteConfirm.bind(this);
  }

  goToPalette(id) {
    //this.props.history.push("/palette/" + id);
    this.props.history.push(`/palette/${id}`);
    //console.log("ssds");
  }

  handleDelete(id){
    this.setState({
      open: true,
      paletteId: id
    })
  }

  deleteConfirm(){
    this.props.deletePalette(this.state.paletteId);
    this.handleCancel();
  }


  handleCancel(){
    this.setState({
      open: false,
      paletteId: ""
    })
  }

  render() {
    //console.log(this.props.palettes);
    const { palettes, classes } = this.props;

    return (
      <div className={classes.root}>
        <div className={classes.container}>
          <nav className={classes.nav}>
            <h1 className={classes.heading}>React Colors</h1>
            <Link to="/palette/new">New Palette</Link>
          </nav>

          <TransitionGroup className={classes.palettes}>
            {palettes.map(palette => (
              <CSSTransition key={palette.id} classNames="fade" timeout={500}>
                <MiniPalette
                  key={palette.id}
                  idPalette={palette.id}
                  {...palette}
                  deletePalette={this.handleDelete}
                  handleClick={this.goToPalette}
                />
              </CSSTransition>
            ))}
          </TransitionGroup>
        </div>
        <Dialog open={this.state.open} aria-labelledby="delete-dialog-title">
          <DialogTitle id="delete-dialog-title">Delete this palette</DialogTitle>
          <List>
            <ListItem button onClick={this.deleteConfirm}>
              <ListItemAvatar>
                <Avatar style={{backgroundColor: blue[100], color: blue[500]}}>
                  <CheckIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary="Delete" />
            </ListItem>
            <ListItem button onClick={this.handleCancel}>
              <ListItemAvatar>
                <Avatar style={{ backgroundColor: red[100], color: red[500]}}>
                  <CloseIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary="Cancel" />
            </ListItem>
          </List>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(styles)(PaletteList);
