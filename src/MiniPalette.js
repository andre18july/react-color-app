import React, { PureComponent } from "react";
import { withStyles } from "@material-ui/styles";
import styles from "./styles/MiniPalette";
import DeleteIcon from "@material-ui/icons/Delete";

class MiniPalette extends PureComponent {

  constructor(props){
    super(props);

    this.handleDeleteClick = this.handleDeleteClick.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  handleDeleteClick(e){
      e.stopPropagation();
      this.props.deletePalette(this.props.idPalette);
  }

  handleClick(){
    this.props.handleClick(this.props.idPalette);
  }


  render() {
    const { classes, paletteName, emoji, colors, idPalette } = this.props;
    console.log("Rend ", idPalette);

    const miniColorBoxes = colors.map(color => (
      <div
        key={color.name}
        className={classes.miniColor}
        style={{ backgroundColor: color.color }}
      />
    ));

    return (
      <div className={classes.root} onClick={this.handleClick}>
        <div className={classes.delete}>
          <DeleteIcon
            onClick={this.handleDeleteClick}
            className={classes.deleteIcon}
            style={{ transition: "all 0.3s ease-in-out" }}
          />
        </div>
        <div className={classes.colors}>{miniColorBoxes}</div>
        <h5 className={classes.title}>
          {paletteName} <span className={classes.emoji}>{emoji}</span>
        </h5>
      </div>
    );
  }
}

export default withStyles(styles)(MiniPalette);
